package motobike.number.plate;

import java.util.List;

import ch.ivyteam.ivy.security.IRole;

public class RolePredicate {

	private static final String POLICEMENT = "Policement";

	public static boolean isPolicement(List<IRole> roles) {
		return isContain(roles, POLICEMENT);
	}

	private static boolean isContain(List<IRole> roles, String roleName) {
		return roles.stream().anyMatch((each) -> each.getName().equals(roleName));
	}
}
