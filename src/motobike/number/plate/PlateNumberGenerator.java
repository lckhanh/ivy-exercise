package motobike.number.plate;

import java.util.Random;
import java.util.function.Consumer;

public class PlateNumberGenerator {
	static final int LETTER_LEFT_LIMIT = 97; // letter 'a'
	static final int LETTER_RIGHT_LIMIT = 122; // letter 'z'

//	public static String generate() {
//		return
//	}
	
	public static void main(String[] args) {
		System.out.println(randomPlateNumber());
	}
	
	/*
	 * Generate random motor bike plate number
	 */
	public static String randomPlateNumber() {
		StringBuilder builder = new StringBuilder();
		
		Consumer<Integer> appendNumber = (length) -> builder.append(randomNumber(length));
		Consumer<Integer> appendString = (length) -> builder.append(randomString(length).toUpperCase());
		
		appendNumber.accept(2);
		appendString.accept(1);
		appendNumber.accept(1);
		builder.append(" - ");
		appendNumber.accept(3);
		builder.append('.');
		appendNumber.accept(2);
		
		return builder.toString();
	}

	/*
	 * Generates a random string by limited length
	 */
	private static String randomString(int length) {
		Random random = new Random();
		StringBuilder buffer = new StringBuilder(length);

		for (int i = 0; i < length; i++) {
			int randomLimitedInt = LETTER_LEFT_LIMIT
					+ (int) (random.nextFloat() * (LETTER_RIGHT_LIMIT - LETTER_LEFT_LIMIT + 1));
			buffer.append((char) randomLimitedInt);
		}
		
		String generatedString = buffer.toString();
		return generatedString;
	}

	/*
	 * Generates a random number
	 */
	private static String randomNumber(int length) {
		Random random = new Random();
		Integer randomInteger = (int) (random.nextFloat() * Math.pow(10, length));
		
		return randomInteger.toString();
	}
	
	
}
