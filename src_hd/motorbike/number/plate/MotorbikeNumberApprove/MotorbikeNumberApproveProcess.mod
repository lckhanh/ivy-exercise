[Ivy]
17E9B24D4A5FA833 7.5.0 #module
>Proto >Proto Collection #zClass
Ms0 MotorbikeNumberApproveProcess Big #zClass
Ms0 RD #cInfo
Ms0 #process
Ms0 @TextInP .type .type #zField
Ms0 @TextInP .processKind .processKind #zField
Ms0 @TextInP .xml .xml #zField
Ms0 @TextInP .responsibility .responsibility #zField
Ms0 @UdInit f0 '' #zField
Ms0 @UdProcessEnd f1 '' #zField
Ms0 @UdEvent f3 '' #zField
Ms0 @UdExitEnd f4 '' #zField
Ms0 @PushWFArc f5 '' #zField
Ms0 @CallSub f16 '' #zField
Ms0 @PushWFArc f17 '' #zField
Ms0 @PushWFArc f2 '' #zField
>Proto Ms0 Ms0 MotorbikeNumberApproveProcess #zField
Ms0 f0 guid 17E9B24D4AC38AB5 #txt
Ms0 f0 method start(motorbike.number.plate.MotorbikePlateRequest) #txt
Ms0 f0 inParameterDecl '<motorbike.number.plate.MotorbikePlateRequest motorbikePlateRequest> param;' #txt
Ms0 f0 inParameterMapAction 'out.motorbikePlateRequest=param.motorbikePlateRequest;
' #txt
Ms0 f0 outParameterDecl '<motorbike.number.plate.MotorbikePlateRequest motorbikePlateRequest> result;' #txt
Ms0 f0 outParameterMapAction 'result.motorbikePlateRequest=in.motorbikePlateRequest;
' #txt
Ms0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(MotorbikePlateRequest)</name>
    </language>
</elementInfo>
' #txt
Ms0 f0 179 51 26 26 -75 15 #rect
Ms0 f0 @|UdInitIcon #fIcon
Ms0 f1 435 51 26 26 0 12 #rect
Ms0 f1 @|UdProcessEndIcon #fIcon
Ms0 f3 guid 17E9B24D4B143343 #txt
Ms0 f3 actionTable 'out=in;
' #txt
Ms0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ms0 f3 179 147 26 26 -14 15 #rect
Ms0 f3 @|UdEventIcon #fIcon
Ms0 f4 307 147 26 26 0 12 #rect
Ms0 f4 @|UdExitEndIcon #fIcon
Ms0 f5 205 160 307 160 #arcP
Ms0 f16 processCall 'Functional Processes/isRolePoliceCheck:call()' #txt
Ms0 f16 requestActionDecl '<> param;' #txt
Ms0 f16 responseMappingAction 'out=in;
' #txt
Ms0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Functional Processes/isRolePoliceCheck</name>
    </language>
</elementInfo>
' #txt
Ms0 f16 208 42 224 44 -107 -8 #rect
Ms0 f16 @|CallSubIcon #fIcon
Ms0 f17 205 64 208 64 #arcP
Ms0 f2 432 64 435 64 #arcP
>Proto Ms0 .type motorbike.number.plate.MotorbikeNumberApprove.MotorbikeNumberApproveData #txt
>Proto Ms0 .processKind HTML_DIALOG #txt
>Proto Ms0 -8 -8 16 16 16 26 #rect
>Proto Ms0 '' #fIcon
Ms0 f3 mainOut f5 tail #connect
Ms0 f5 head f4 mainIn #connect
Ms0 f0 mainOut f17 tail #connect
Ms0 f17 head f16 mainIn #connect
Ms0 f16 mainOut f2 tail #connect
Ms0 f2 head f1 mainIn #connect
