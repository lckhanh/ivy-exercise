[Ivy]
17E9484593212C0E 7.5.0 #module
>Proto >Proto Collection #zClass
Ms0 MotorbikeNumberRequestListProcess Big #zClass
Ms0 RD #cInfo
Ms0 #process
Ms0 @TextInP .type .type #zField
Ms0 @TextInP .processKind .processKind #zField
Ms0 @TextInP .xml .xml #zField
Ms0 @TextInP .responsibility .responsibility #zField
Ms0 @UdInit f0 '' #zField
Ms0 @UdProcessEnd f1 '' #zField
Ms0 @UdEvent f3 '' #zField
Ms0 @UdExitEnd f4 '' #zField
Ms0 @PushWFArc f5 '' #zField
Ms0 @GridStep f6 '' #zField
Ms0 @UdMethod f13 '' #zField
Ms0 @CallSub f12 '' #zField
Ms0 @PushWFArc f14 '' #zField
Ms0 @UdProcessEnd f15 '' #zField
Ms0 @PushWFArc f8 '' #zField
Ms0 @EMail f20 '' #zField
Ms0 @UdMethod f9 '' #zField
Ms0 @UdMethod f11 '' #zField
Ms0 @GridStep f10 '' #zField
Ms0 @EMail f18 '' #zField
Ms0 @GridStep f19 '' #zField
Ms0 @PushWFArc f21 '' #zField
Ms0 @PushWFArc f24 '' #zField
Ms0 @PushWFArc f25 '' #zField
Ms0 @PushWFArc f26 '' #zField
Ms0 @PushWFArc f17 '' #zField
Ms0 @PushWFArc f22 '' #zField
Ms0 @PushWFArc f23 '' #zField
Ms0 @PushWFArc f2 '' #zField
>Proto Ms0 Ms0 MotorbikeNumberRequestListProcess #zField
Ms0 f0 guid 17E94845936F8B10 #txt
Ms0 f0 method start() #txt
Ms0 f0 inParameterDecl '<> param;' #txt
Ms0 f0 outParameterDecl '<> result;' #txt
Ms0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
    </language>
</elementInfo>
' #txt
Ms0 f0 83 51 26 26 -15 15 #rect
Ms0 f0 @|UdInitIcon #fIcon
Ms0 f1 651 51 26 26 0 12 #rect
Ms0 f1 @|UdProcessEndIcon #fIcon
Ms0 f3 guid 17E9484593AC2F1B #txt
Ms0 f3 actionTable 'out=in;
' #txt
Ms0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ms0 f3 83 147 26 26 -14 15 #rect
Ms0 f3 @|UdEventIcon #fIcon
Ms0 f4 315 147 26 26 0 12 #rect
Ms0 f4 @|UdExitEndIcon #fIcon
Ms0 f5 109 160 315 160 #arcP
Ms0 f6 actionTable 'out=in;
' #txt
Ms0 f6 actionCode 'out.motorbikePlateNumberRequests = 
	ivy.persistence.myPersistentUnit.createQuery("select r from MotorbikePlateRequest r where r.deletedAt is null and r.approvalDate is null").getResultList();
	
ivy.log.info(out.motorbikePlateNumberRequests);' #txt
Ms0 f6 512 42 112 44 0 -8 #rect
Ms0 f6 @|StepIcon #fIcon
Ms0 f13 guid 17E9B07F14446406 #txt
Ms0 f13 method view(motorbike.number.plate.MotorbikePlateRequest) #txt
Ms0 f13 inParameterDecl '<motorbike.number.plate.MotorbikePlateRequest selectedRequest> param;' #txt
Ms0 f13 inActionCode 'out.selectedRequest = param.selectedRequest;' #txt
Ms0 f13 outParameterDecl '<String destination> result;' #txt
Ms0 f13 outParameterMapAction 'result.destination="MotorbikeNumberRequestDetail";
' #txt
Ms0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>view(MotorbikePlateRequest)</name>
    </language>
</elementInfo>
' #txt
Ms0 f13 83 243 26 26 -23 15 #rect
Ms0 f13 @|UdMethodIcon #fIcon
Ms0 f12 processCall 'Functional Processes/isRolePoliceCheck:call()' #txt
Ms0 f12 requestActionDecl '<> param;' #txt
Ms0 f12 responseMappingAction 'out=in;
' #txt
Ms0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Functional Processes/isRolePoliceCheck</name>
    </language>
</elementInfo>
' #txt
Ms0 f12 128 42 224 44 -107 -8 #rect
Ms0 f12 @|CallSubIcon #fIcon
Ms0 f14 109 64 128 64 #arcP
Ms0 f15 435 243 26 26 0 12 #rect
Ms0 f15 @|UdProcessEndIcon #fIcon
Ms0 f8 109 256 435 256 #arcP
Ms0 f20 beanConfig '"{/emailSubject ""Your request is approved""/emailFrom ""noreply@localhost""/emailReplyTo """"/emailTo ""<%= in.selectedRequest.bikeOwner.email  %>""/emailCC """"/emailBCC """"/exceptionMissingEmailAttachments ""false""/emailMessage ""Your request is approved.\\nYour plate number: <%= in.selectedRequest.motorbike.plateNumber  %>""/emailAttachments * }"' #txt
Ms0 f20 type motorbike.number.plate.MotorbikeNumberRequestList.MotorbikeNumberRequestListData #txt
Ms0 f20 timeout 0 #txt
Ms0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Your request is approved</name>
    </language>
</elementInfo>
' #txt
Ms0 f20 312 434 144 44 -65 -8 #rect
Ms0 f20 @|EMailIcon #fIcon
Ms0 f9 guid 17ED3CDEA18CD4BE #txt
Ms0 f9 method rejectNumberPlateRequest() #txt
Ms0 f9 inParameterDecl '<> param;' #txt
Ms0 f9 inActionCode ivy.log.info("reject"); #txt
Ms0 f9 outParameterDecl '<String destination> result;' #txt
Ms0 f9 outParameterMapAction 'result.destination="MotorbikeNumberRequestList";
' #txt
Ms0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>rejectNumberPlateRequest()</name>
    </language>
</elementInfo>
' #txt
Ms0 f9 83 347 26 26 -64 22 #rect
Ms0 f9 @|UdMethodIcon #fIcon
Ms0 f11 guid 17ED3CDEA19820BC #txt
Ms0 f11 method approveNumberPlateRequest() #txt
Ms0 f11 inParameterDecl '<> param;' #txt
Ms0 f11 inActionCode ivy.log.info("approve"); #txt
Ms0 f11 outParameterDecl '<String destination> result;' #txt
Ms0 f11 outParameterMapAction 'result.destination="MotorbikeNumberRequestList";
' #txt
Ms0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>approveNumberPlateRequest()</name>
    </language>
</elementInfo>
' #txt
Ms0 f11 83 443 26 26 -65 20 #rect
Ms0 f11 @|UdMethodIcon #fIcon
Ms0 f10 actionTable 'out=in;
' #txt
Ms0 f10 actionCode 'out.selectedRequest.approvalDate = new Date();' #txt
Ms0 f10 168 434 112 44 0 -8 #rect
Ms0 f10 @|StepIcon #fIcon
Ms0 f18 beanConfig '"{/emailSubject ""Your request is rejected""/emailFrom ""noreply@localhost""/emailReplyTo """"/emailTo ""<%= in.selectedRequest.bikeOwner.email  %>""/emailCC """"/emailBCC """"/exceptionMissingEmailAttachments ""false""/emailMessage ""Your request is rejected""/emailAttachments * }"' #txt
Ms0 f18 type motorbike.number.plate.MotorbikeNumberRequestList.MotorbikeNumberRequestListData #txt
Ms0 f18 timeout 0 #txt
Ms0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Your request is rejected</name>
    </language>
</elementInfo>
' #txt
Ms0 f18 312 336 144 48 -61 -8 #rect
Ms0 f18 @|EMailIcon #fIcon
Ms0 f19 actionTable 'out=in;
' #txt
Ms0 f19 actionCode 'out.selectedRequest.approvalDate = new Date();

ivy.persistence.myPersistentUnit.merge(out.selectedRequest);' #txt
Ms0 f19 168 338 112 44 0 -8 #rect
Ms0 f19 @|StepIcon #fIcon
Ms0 f21 280 456 312 456 #arcP
Ms0 f24 109 456 168 456 #arcP
Ms0 f25 109 360 168 360 #arcP
Ms0 f26 280 360 312 360 #arcP
Ms0 f17 624 64 651 64 #arcP
Ms0 f22 expr out #txt
Ms0 f22 456 360 512 64 #arcP
Ms0 f22 1 480 360 #addKink
Ms0 f22 2 480 64 #addKink
Ms0 f22 1 0.4864864864864865 0 0 #arcLabel
Ms0 f23 expr out #txt
Ms0 f23 456 456 512 64 #arcP
Ms0 f23 1 480 456 #addKink
Ms0 f23 2 480 64 #addKink
Ms0 f23 1 0.5 0 0 #arcLabel
Ms0 f2 352 64 512 64 #arcP
>Proto Ms0 .type motorbike.number.plate.MotorbikeNumberRequestList.MotorbikeNumberRequestListData #txt
>Proto Ms0 .processKind HTML_DIALOG #txt
>Proto Ms0 -8 -8 16 16 16 26 #rect
>Proto Ms0 '' #fIcon
Ms0 f3 mainOut f5 tail #connect
Ms0 f5 head f4 mainIn #connect
Ms0 f0 mainOut f14 tail #connect
Ms0 f14 head f12 mainIn #connect
Ms0 f13 mainOut f8 tail #connect
Ms0 f8 head f15 mainIn #connect
Ms0 f19 mainOut f26 tail #connect
Ms0 f26 head f18 mainIn #connect
Ms0 f10 mainOut f21 tail #connect
Ms0 f21 head f20 mainIn #connect
Ms0 f11 mainOut f24 tail #connect
Ms0 f24 head f10 mainIn #connect
Ms0 f9 mainOut f25 tail #connect
Ms0 f25 head f19 mainIn #connect
Ms0 f6 mainOut f17 tail #connect
Ms0 f17 head f1 mainIn #connect
Ms0 f18 mainOut f22 tail #connect
Ms0 f22 head f6 mainIn #connect
Ms0 f20 mainOut f23 tail #connect
Ms0 f23 head f6 mainIn #connect
Ms0 f12 mainOut f2 tail #connect
Ms0 f2 head f6 mainIn #connect
