[Ivy]
17E9EF9C11241C03 7.5.0 #module
>Proto >Proto Collection #zClass
Ls0 LoginProcess Big #zClass
Ls0 RD #cInfo
Ls0 #process
Ls0 @TextInP .type .type #zField
Ls0 @TextInP .processKind .processKind #zField
Ls0 @TextInP .xml .xml #zField
Ls0 @TextInP .responsibility .responsibility #zField
Ls0 @UdInit f0 '' #zField
Ls0 @UdProcessEnd f1 '' #zField
Ls0 @PushWFArc f2 '' #zField
Ls0 @UdEvent f3 '' #zField
Ls0 @UdExitEnd f4 '' #zField
Ls0 @PushWFArc f5 '' #zField
Ls0 @UdMethod f6 '' #zField
Ls0 @GridStep f9 '' #zField
Ls0 @PushWFArc f10 '' #zField
Ls0 @UdExitEnd f7 '' #zField
Ls0 @PushWFArc f8 '' #zField
>Proto Ls0 Ls0 LoginProcess #zField
Ls0 f0 guid 17E9EF9C11DE9E88 #txt
Ls0 f0 method start(motorbike.number.plate.User) #txt
Ls0 f0 inParameterDecl '<motorbike.number.plate.User user> param;' #txt
Ls0 f0 inParameterMapAction 'out.user=param.user;
' #txt
Ls0 f0 outParameterDecl '<motorbike.number.plate.User user> result;' #txt
Ls0 f0 outParameterMapAction 'result.user=in.user;
' #txt
Ls0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(User)</name>
    </language>
</elementInfo>
' #txt
Ls0 f0 83 51 26 26 -28 15 #rect
Ls0 f0 @|UdInitIcon #fIcon
Ls0 f1 211 51 26 26 0 12 #rect
Ls0 f1 @|UdProcessEndIcon #fIcon
Ls0 f2 109 64 211 64 #arcP
Ls0 f3 guid 17E9EF9C129D8291 #txt
Ls0 f3 actionTable 'out=in;
' #txt
Ls0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ls0 f3 83 147 26 26 -14 15 #rect
Ls0 f3 @|UdEventIcon #fIcon
Ls0 f4 211 147 26 26 0 12 #rect
Ls0 f4 @|UdExitEndIcon #fIcon
Ls0 f5 109 160 211 160 #arcP
Ls0 f6 guid 17E9EFDDAF276D69 #txt
Ls0 f6 method login() #txt
Ls0 f6 inParameterDecl '<> param;' #txt
Ls0 f6 outParameterDecl '<> result;' #txt
Ls0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>login()</name>
    </language>
</elementInfo>
' #txt
Ls0 f6 83 243 26 26 -23 15 #rect
Ls0 f6 @|UdMethodIcon #fIcon
Ls0 f9 actionTable 'out=in;
' #txt
Ls0 f9 actionCode 'ivy.session.loginSessionUser(out.username, out.passwords);' #txt
Ls0 f9 168 234 112 44 0 -8 #rect
Ls0 f9 @|StepIcon #fIcon
Ls0 f10 109 256 168 256 #arcP
Ls0 f7 339 243 26 26 0 12 #rect
Ls0 f7 @|UdExitEndIcon #fIcon
Ls0 f8 280 256 339 256 #arcP
>Proto Ls0 .type motorbike.number.plate.Login.LoginData #txt
>Proto Ls0 .processKind HTML_DIALOG #txt
>Proto Ls0 -8 -8 16 16 16 26 #rect
>Proto Ls0 '' #fIcon
Ls0 f0 mainOut f2 tail #connect
Ls0 f2 head f1 mainIn #connect
Ls0 f3 mainOut f5 tail #connect
Ls0 f5 head f4 mainIn #connect
Ls0 f6 mainOut f10 tail #connect
Ls0 f10 head f9 mainIn #connect
Ls0 f9 mainOut f8 tail #connect
Ls0 f8 head f7 mainIn #connect
