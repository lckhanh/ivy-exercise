[Ivy]
17E946BF907CFB44 7.5.0 #module
>Proto >Proto Collection #zClass
Ms0 MotorbikeNumberRequestProcess Big #zClass
Ms0 RD #cInfo
Ms0 #process
Ms0 @TextInP .type .type #zField
Ms0 @TextInP .processKind .processKind #zField
Ms0 @TextInP .xml .xml #zField
Ms0 @TextInP .responsibility .responsibility #zField
Ms0 @UdInit f0 '' #zField
Ms0 @UdProcessEnd f1 '' #zField
Ms0 @PushWFArc f2 '' #zField
Ms0 @UdEvent f3 '' #zField
Ms0 @UdExitEnd f4 '' #zField
Ms0 @PushWFArc f5 '' #zField
Ms0 @UdEvent f6 '' #zField
Ms0 @GridStep f7 '' #zField
Ms0 @UdProcessEnd f8 '' #zField
Ms0 @PushWFArc f9 '' #zField
Ms0 @PushWFArc f10 '' #zField
Ms0 @UdEvent f11 '' #zField
Ms0 @GridStep f12 '' #zField
Ms0 @PushWFArc f13 '' #zField
Ms0 @UdProcessEnd f14 '' #zField
Ms0 @PushWFArc f15 '' #zField
Ms0 @ErrorBoundaryEvent f16 '' #zField
Ms0 @PushWFArc f17 '' #zField
>Proto Ms0 Ms0 MotorbikeNumberRequestProcess #zField
Ms0 f0 guid 17E946BF9129C382 #txt
Ms0 f0 method start(motorbike.number.plate.Data) #txt
Ms0 f0 inParameterDecl '<motorbike.number.plate.Data data> param;' #txt
Ms0 f0 inParameterMapAction 'out.motorbikePlateRequest=param.data.motorbikePlateRequest;
' #txt
Ms0 f0 outParameterDecl '<motorbike.number.plate.MotorbikePlateRequest data> result;' #txt
Ms0 f0 outParameterMapAction 'result.data=in.motorbikePlateRequest;
' #txt
Ms0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(Data)</name>
    </language>
</elementInfo>
' #txt
Ms0 f0 83 51 26 26 -28 15 #rect
Ms0 f0 @|UdInitIcon #fIcon
Ms0 f1 211 51 26 26 0 12 #rect
Ms0 f1 @|UdProcessEndIcon #fIcon
Ms0 f2 109 64 211 64 #arcP
Ms0 f3 guid 17E946BF91F64605 #txt
Ms0 f3 actionTable 'out=in;
' #txt
Ms0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ms0 f3 83 147 26 26 -14 15 #rect
Ms0 f3 @|UdEventIcon #fIcon
Ms0 f4 211 147 26 26 0 12 #rect
Ms0 f4 @|UdExitEndIcon #fIcon
Ms0 f5 109 160 211 160 #arcP
Ms0 f6 guid 17E95B91233A947B #txt
Ms0 f6 actionTable 'out=in;
' #txt
Ms0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>submit</name>
    </language>
</elementInfo>
' #txt
Ms0 f6 83 211 26 26 -14 15 #rect
Ms0 f6 @|UdEventIcon #fIcon
Ms0 f7 actionTable 'out=in;
' #txt
Ms0 f7 actionCode 'out.motorbikePlateRequest.createdAt = new Date();
ivy.persistence.myPersistentUnit.merge(out.motorbikePlateRequest);' #txt
Ms0 f7 160 202 112 44 0 -8 #rect
Ms0 f7 @|StepIcon #fIcon
Ms0 f8 355 211 26 26 0 12 #rect
Ms0 f8 @|UdProcessEndIcon #fIcon
Ms0 f9 109 224 160 224 #arcP
Ms0 f10 272 224 355 224 #arcP
Ms0 f11 guid 17E99B11509C8872 #txt
Ms0 f11 actionTable 'out=in;
' #txt
Ms0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>generate</name>
    </language>
</elementInfo>
' #txt
Ms0 f11 83 331 26 26 -14 15 #rect
Ms0 f11 @|UdEventIcon #fIcon
Ms0 f12 actionTable 'out=in;
' #txt
Ms0 f12 actionCode 'import motobike.number.plate.PlateNumberGenerator;

out.motorbikePlateRequest.motorbike.plateNumber = PlateNumberGenerator.randomPlateNumber();
' #txt
Ms0 f12 160 322 112 44 0 -8 #rect
Ms0 f12 @|StepIcon #fIcon
Ms0 f13 109 344 160 344 #arcP
Ms0 f14 307 331 26 26 0 12 #rect
Ms0 f14 @|UdProcessEndIcon #fIcon
Ms0 f15 272 344 307 344 #arcP
Ms0 f16 actionTable 'out=in;
' #txt
Ms0 f16 actionCode ivy.log.fatal(error.stackTrace.toString()); #txt
Ms0 f16 errorCode ivy:error:persistence #txt
Ms0 f16 attachedToRef 17E946BF907CFB44-f7 #txt
Ms0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>ivy:error:persistence</name>
    </language>
</elementInfo>
' #txt
Ms0 f16 233 241 30 30 -47 18 #rect
Ms0 f16 @|ErrorBoundaryEventIcon #fIcon
Ms0 f17 262 252 355 227 #arcP
>Proto Ms0 .type motorbike.number.plate.MotorbikeNumberRequest.MotorbikeNumberRequestData #txt
>Proto Ms0 .processKind HTML_DIALOG #txt
>Proto Ms0 -8 -8 16 16 16 26 #rect
>Proto Ms0 '' #fIcon
Ms0 f0 mainOut f2 tail #connect
Ms0 f2 head f1 mainIn #connect
Ms0 f3 mainOut f5 tail #connect
Ms0 f5 head f4 mainIn #connect
Ms0 f6 mainOut f9 tail #connect
Ms0 f9 head f7 mainIn #connect
Ms0 f7 mainOut f10 tail #connect
Ms0 f10 head f8 mainIn #connect
Ms0 f11 mainOut f13 tail #connect
Ms0 f13 head f12 mainIn #connect
Ms0 f12 mainOut f15 tail #connect
Ms0 f15 head f14 mainIn #connect
Ms0 f16 mainOut f17 tail #connect
Ms0 f17 head f8 mainIn #connect
