[Ivy]
17E947A8CB88E4E6 7.5.0 #module
>Proto >Proto Collection #zClass
ae0 approveMotobikeNumberPlate Big #zClass
ae0 B #cInfo
ae0 #process
ae0 @TextInP .type .type #zField
ae0 @TextInP .processKind .processKind #zField
ae0 @TextInP .xml .xml #zField
ae0 @TextInP .responsibility .responsibility #zField
ae0 @StartRequest f0 '' #zField
ae0 @EndTask f1 '' #zField
ae0 @UserDialog f3 '' #zField
ae0 @PushWFArc f2 '' #zField
ae0 @CallSub f5 '' #zField
ae0 @PushWFArc f6 '' #zField
ae0 @PushWFArc f4 '' #zField
>Proto ae0 ae0 approveMotobikeNumberPlate #zField
ae0 f0 outLink start.ivp #txt
ae0 f0 inParamDecl '<> param;' #txt
ae0 f0 requestEnabled true #txt
ae0 f0 triggerEnabled false #txt
ae0 f0 callSignature start() #txt
ae0 f0 caseData businessCase.attach=true #txt
ae0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
ae0 f0 @C|.responsibility Everybody #txt
ae0 f0 81 49 30 30 -20 17 #rect
ae0 f0 @|StartRequestIcon #fIcon
ae0 f1 537 49 30 30 0 15 #rect
ae0 f1 @|EndIcon #fIcon
ae0 f3 dialogId motorbike.number.plate.MotorbikeNumberRequestList #txt
ae0 f3 startMethod start() #txt
ae0 f3 requestActionDecl '<> param;' #txt
ae0 f3 responseMappingAction 'out=in;
' #txt
ae0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>MotorbikeNumberRequestList</name>
    </language>
</elementInfo>
' #txt
ae0 f3 344 42 160 44 -77 -8 #rect
ae0 f3 @|UserDialogIcon #fIcon
ae0 f2 504 64 537 64 #arcP
ae0 f5 processCall 'Functional Processes/isRolePoliceCheck:call()' #txt
ae0 f5 requestActionDecl '<> param;' #txt
ae0 f5 responseMappingAction 'out=in;
' #txt
ae0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Functional Processes/LoginCheck</name>
    </language>
</elementInfo>
' #txt
ae0 f5 128 42 192 44 -89 -8 #rect
ae0 f5 @|CallSubIcon #fIcon
ae0 f6 111 64 128 64 #arcP
ae0 f4 320 64 344 64 #arcP
>Proto ae0 .type motorbike.number.plate.MotorbikePlateRequest #txt
>Proto ae0 .processKind NORMAL #txt
>Proto ae0 0 0 32 24 18 0 #rect
>Proto ae0 @|BIcon #fIcon
ae0 f3 mainOut f2 tail #connect
ae0 f2 head f1 mainIn #connect
ae0 f0 mainOut f6 tail #connect
ae0 f6 head f5 mainIn #connect
ae0 f5 mainOut f4 tail #connect
ae0 f4 head f3 mainIn #connect
