[Ivy]
17E9A7FC61950DA7 7.5.0 #module
>Proto >Proto Collection #zClass
re0 requestMotorNumberPlate Big #zClass
re0 B #cInfo
re0 #process
re0 @TextInP .type .type #zField
re0 @TextInP .processKind .processKind #zField
re0 @TextInP .xml .xml #zField
re0 @TextInP .responsibility .responsibility #zField
re0 @StartRequest f0 '' #zField
re0 @EndTask f1 '' #zField
re0 @UserDialog f3 '' #zField
re0 @PushWFArc f4 '' #zField
re0 @PushWFArc f2 '' #zField
>Proto re0 re0 requestMotorNumberPlate #zField
re0 f0 outLink start.ivp #txt
re0 f0 inParamDecl '<> param;' #txt
re0 f0 requestEnabled true #txt
re0 f0 triggerEnabled false #txt
re0 f0 callSignature start() #txt
re0 f0 caseData businessCase.attach=true #txt
re0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
re0 f0 @C|.responsibility Everybody #txt
re0 f0 81 49 30 30 -20 17 #rect
re0 f0 @|StartRequestIcon #fIcon
re0 f1 337 49 30 30 0 15 #rect
re0 f1 @|EndIcon #fIcon
re0 f3 dialogId motorbike.number.plate.MotorbikeNumberRequest #txt
re0 f3 startMethod start(motorbike.number.plate.Data) #txt
re0 f3 requestActionDecl '<motorbike.number.plate.Data data> param;' #txt
re0 f3 responseMappingAction 'out=in;
' #txt
re0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>MotorbikeNumberRequest</name>
    </language>
</elementInfo>
' #txt
re0 f3 152 42 144 44 -67 -8 #rect
re0 f3 @|UserDialogIcon #fIcon
re0 f4 111 64 152 64 #arcP
re0 f2 296 64 337 64 #arcP
>Proto re0 .type motorbike.number.plate.MotorbikePlateRequest #txt
>Proto re0 .processKind NORMAL #txt
>Proto re0 0 0 32 24 18 0 #rect
>Proto re0 @|BIcon #fIcon
re0 f0 mainOut f4 tail #connect
re0 f4 head f3 mainIn #connect
re0 f3 mainOut f2 tail #connect
re0 f2 head f1 mainIn #connect
