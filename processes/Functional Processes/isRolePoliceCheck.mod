[Ivy]
17EA00720DCC461A 7.5.0 #module
>Proto >Proto Collection #zClass
Lk0 isRolePoliceCheck Big #zClass
Lk0 B #cInfo
Lk0 #process
Lk0 @TextInP .type .type #zField
Lk0 @TextInP .processKind .processKind #zField
Lk0 @TextInP .xml .xml #zField
Lk0 @TextInP .responsibility .responsibility #zField
Lk0 @StartSub f0 '' #zField
Lk0 @EndSub f1 '' #zField
Lk0 @Alternative f5 '' #zField
Lk0 @UserDialog f10 '' #zField
Lk0 @GridStep f7 '' #zField
Lk0 @PushWFArc f11 '' #zField
Lk0 @PushWFArc f9 '' #zField
Lk0 @PushWFArc f6 '' #zField
Lk0 @PushWFArc f2 '' #zField
Lk0 @PushWFArc f3 '' #zField
>Proto Lk0 Lk0 isRolePoliceCheck #zField
Lk0 f0 inParamDecl '<> param;' #txt
Lk0 f0 outParamDecl '<> result;' #txt
Lk0 f0 callSignature call() #txt
Lk0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
    </language>
</elementInfo>
' #txt
Lk0 f0 81 49 30 30 -13 17 #rect
Lk0 f0 @|StartSubIcon #fIcon
Lk0 f1 401 49 30 30 0 15 #rect
Lk0 f1 @|EndSubIcon #fIcon
Lk0 f5 344 48 32 32 0 16 #rect
Lk0 f5 @|AlternativeIcon #fIcon
Lk0 f10 dialogId motorbike.number.plate.Login #txt
Lk0 f10 startMethod start(motorbike.number.plate.User) #txt
Lk0 f10 requestActionDecl '<motorbike.number.plate.User user> param;' #txt
Lk0 f10 responseMappingAction 'out=in;
' #txt
Lk0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Login</name>
    </language>
</elementInfo>
' #txt
Lk0 f10 192 146 112 44 -14 -8 #rect
Lk0 f10 @|UserDialogIcon #fIcon
Lk0 f7 actionTable 'out=in;
' #txt
Lk0 f7 128 42 112 44 0 -8 #rect
Lk0 f7 @|StepIcon #fIcon
Lk0 f11 expr in #txt
Lk0 f11 outCond 'ivy.session.getSessionUser() == null || !motobike.number.plate.RolePredicate.isPolicement(ivy.session.getSessionUser().getAllRoles())' #txt
Lk0 f11 352 72 248 146 #arcP
Lk0 f11 0 0.6842105263157896 0 0 #arcLabel
Lk0 f9 248 146 184 86 #arcP
Lk0 f6 240 64 344 64 #arcP
Lk0 f2 111 64 128 64 #arcP
Lk0 f3 376 64 401 64 #arcP
>Proto Lk0 .type motorbike.number.plate.LoginCheckData #txt
>Proto Lk0 .processKind CALLABLE_SUB #txt
>Proto Lk0 0 0 32 24 18 0 #rect
>Proto Lk0 @|BIcon #fIcon
Lk0 f5 out f11 tail #connect
Lk0 f11 head f10 mainIn #connect
Lk0 f7 mainOut f6 tail #connect
Lk0 f6 head f5 in #connect
Lk0 f10 mainOut f9 tail #connect
Lk0 f9 head f7 mainIn #connect
Lk0 f0 mainOut f2 tail #connect
Lk0 f2 head f7 mainIn #connect
Lk0 f5 out f3 tail #connect
Lk0 f3 head f1 mainIn #connect
